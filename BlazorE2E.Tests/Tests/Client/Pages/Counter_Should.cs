﻿using System;
using System.Threading.Tasks;
using Microsoft.Playwright;
using Xunit;

namespace BlazorE2E.Tests.Tests.Client.Pages
{
	public class Counter_Should
	{
        [Fact]
		public async Task Increase_On_Click()
        {
			using var playwright = await Playwright.CreateAsync();
            await using var browser = await playwright.Chromium.LaunchAsync(new BrowserTypeLaunchOptions()
            {
                Headless = false,
                SlowMo = 1000
            });
			var page = await browser.NewPageAsync();

            // Go to https://localhost:7108/
            await page.GotoAsync("https://localhost:7108/");
            // Click text=Counter
            await page.ClickAsync("text=Counter");
            // Assert.AreEqual("https://localhost:7108/counter", page.Url);

            Assert.Equal(await page.TextContentAsync("[pw-name='counter']"), "Current count: 0");

            // Click text=Click me
            await page.ClickAsync("text=Click me");

            Assert.Equal(await page.TextContentAsync("[pw-name='counter']"), "Current count: 1");
        }
	}
}

